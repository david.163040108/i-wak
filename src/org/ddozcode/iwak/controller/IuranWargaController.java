/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ddozcode.iwak.model.data.IuranWargaModel;
import org.ddozcode.iwak.model.data.WargaModel;
import org.ddozcode.iwak.model.pojo.StatusIuran;
import org.ddozcode.iwak.model.pojo.Warga;
import org.ddozcode.iwak.utilities.DatabaseUtilities;

/**
 *
 * @author User
 */
public class IuranWargaController {

    public int iuran(StatusIuran warga) throws SQLException {
        IuranWargaModel model = new IuranWargaModel();
        return model.iuran(warga);
    }
    

    public List<StatusIuran> loadWargaSudahBayar() throws SQLException {
        IuranWargaModel model = new IuranWargaModel();
        return model.loadWargaSudahBayar();
    }

    public List<StatusIuran> loadWargaBelumBayar() throws SQLException {
        IuranWargaModel model = new IuranWargaModel();
        return model.loadWargaBelumBayar();
    }

    public int ubahStatusIuran(StatusIuran si) throws SQLException {
        IuranWargaModel model = new IuranWargaModel();
        return model.ubahIuranWarga(si);

    }

}
