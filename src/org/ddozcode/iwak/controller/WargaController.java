
package org.ddozcode.iwak.controller;

import java.sql.SQLException;
import java.util.List;
import org.ddozcode.iwak.model.data.WargaModel;
import org.ddozcode.iwak.model.pojo.Warga;

/**
 *
 * @author User
 */
public class WargaController {

    public List<Warga> loadDataWarga() throws SQLException {
        WargaModel model = new WargaModel();
        return model.loadDataWarga();
    }
    
    public List<Warga> loadKtp() throws SQLException {
        WargaModel model = new WargaModel();
        return model.loadktp();
    }

    public int tambahDataWarga(Warga warga) throws SQLException {
        WargaModel model = new WargaModel();
        return model.tambahDataWarga(warga);
    }

    public int hapusDataWarga(Warga warga) throws SQLException {
        WargaModel model = new WargaModel();
        return model.hapusDataWarga(warga);
    }

     public int ubahDataWarga(Warga warga) throws SQLException {
        WargaModel model = new WargaModel();
        return model.ubahDataWarga(warga);
    }

}
