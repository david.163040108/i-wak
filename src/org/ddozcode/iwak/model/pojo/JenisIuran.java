/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.model.pojo;

/**
 *
 * @author User
 */
public class JenisIuran {
    private String idJenisIuran;
    private String namaIuran;
    private int biaya;

    public JenisIuran(String idJenisIuran) {
        this.idJenisIuran = idJenisIuran;
    }

    public JenisIuran() {
   
    }
    
    
    public String getIdJenisIuran() {
        return idJenisIuran;
    }

    public void setIdJenisIuran(String idJenisIuran) {
        this.idJenisIuran = idJenisIuran;
    }

    public String getNamaIuran() {
        return namaIuran;
    }

    public void setNamaIuran(String namaIuran) {
        this.namaIuran = namaIuran;
    }

    public int getBiaya() {
        return biaya;
    }

    public void setBiaya(int biaya) {
        this.biaya = biaya;
    }
    
    
}
