/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.model.pojo;

/**
 *
 * @author User
 */
public class Warga {
    private String ktp;
    private String nama;
    private String noRumah;
    private String noHp;
    private String id;

    public Warga() {
    }
    
    public Warga(String ktp, String nama, String noRumah, String noHp) {
        this.ktp = ktp;
        this.nama = nama;
        this.noRumah = noRumah;
        this.noHp = noHp;
    }

    public Warga(String ktp) {
        this.ktp = ktp;
    }

    public Warga(String ktp, String nama, String noRumah, String noHp, String id) {
        this.ktp = ktp;
        this.nama = nama;
        this.noRumah = noRumah;
        this.noHp = noHp;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getKtp() {
        return ktp;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoRumah() {
        return noRumah;
    }

    public void setNoRumah(String noRumah) {
        this.noRumah = noRumah;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
    
    
}
