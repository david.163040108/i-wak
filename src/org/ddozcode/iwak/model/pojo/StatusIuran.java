/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.model.pojo;

import java.util.Date;

/**
 *
 * @author User
 */
public class StatusIuran {
    private int idStatusIuran;
    private Warga ktp;
    private JenisIuran jenisIuran;
    private String tanggal;
    private String status;

    public StatusIuran() {

    }


    public StatusIuran(String tanggal, String status, int idStatusIuran){
        this.tanggal =tanggal;
        this.status = status;
        this.idStatusIuran = idStatusIuran;
    }
    
    public StatusIuran(Warga ktp, JenisIuran jenisIuran, String tanggal, String status) {
        this.ktp = ktp;
        this.jenisIuran = jenisIuran;
        this.tanggal = tanggal;
        this.status = status;
    }

    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public Warga getKtp() {
        return ktp;
    }

    public void setKtp(Warga ktp) {
        this.ktp = ktp;
    }

    public JenisIuran getJenisIuran() {
        return jenisIuran;
    }

    public void setJenisIuran(JenisIuran jenisIuran) {
        this.jenisIuran = jenisIuran;
    }

    public int getIdStatusIuran() {
        return idStatusIuran;
    }

    public void setIdStatusIuran(int idStatusIuran) {
        this.idStatusIuran = idStatusIuran;
    }
    
    

}
