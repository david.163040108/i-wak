/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.model.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ddozcode.iwak.model.pojo.Warga;
import org.ddozcode.iwak.utilities.DatabaseUtilities;

/**
 *
 * @author User
 */
public class WargaModel {

    public List<Warga> loadDataWarga() throws SQLException {
        List<Warga> wrgList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM warga");
            wrgList = new ArrayList<>();
            while (rs.next()) {
                Warga wrg = new Warga();
                wrg.setKtp(rs.getString("ktp"));
                wrg.setNama(rs.getString("nama"));
                wrg.setNoRumah(rs.getString("noRumah"));
                wrg.setNoHp(rs.getString("noHp"));
                wrgList.add(wrg);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return wrgList;
    }

    public int tambahDataWarga(Warga wrg) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("INSERT INTO warga value(?,?,?,?)");
            stat.setString(1, wrg.getKtp());
            stat.setString(2, wrg.getNama());
            stat.setString(3, wrg.getNoRumah());
            stat.setString(4, wrg.getNoHp());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public int hapusDataWarga(Warga wrg) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {

            PreparedStatement stat = con.prepareStatement("DELETE FROM warga WHERE ktp = ?");
            stat.setString(1, wrg.getKtp());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public int ubahDataWarga(Warga wrg) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {

            PreparedStatement stat = con.prepareStatement("UPDATE warga SET ktp = ? ,nama = ?, noRumah =?, noHp = ? WHERE ktp = ? ");
            stat.setString(1, wrg.getKtp());
            stat.setString(2, wrg.getNama());
            stat.setString(3, wrg.getNoRumah());
            stat.setString(4, wrg.getNoHp());
            stat.setString(5, wrg.getId());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public List<Warga> loadktp() throws SQLException {
        List<Warga> wrgList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT `ktp` FROM `warga`");
            wrgList = new ArrayList<>();
            while (rs.next()) {
                Warga wrg = new Warga();
                wrg.setKtp(rs.getString("ktp"));
                wrgList.add(wrg);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return wrgList;
    }

}
