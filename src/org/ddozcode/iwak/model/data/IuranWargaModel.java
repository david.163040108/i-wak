/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ddozcode.iwak.model.data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ddozcode.iwak.model.pojo.JenisIuran;
import org.ddozcode.iwak.model.pojo.StatusIuran;
import org.ddozcode.iwak.model.pojo.Warga;
import org.ddozcode.iwak.utilities.DatabaseUtilities;

/**
 *
 * @author User
 */
public class IuranWargaModel {

    public int iuran(StatusIuran wrg) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("INSERT INTO statusiuran value(null,?,?,?,?)");
            stat.setString(1, wrg.getJenisIuran().getIdJenisIuran());
            stat.setString(2, wrg.getTanggal());
            stat.setString(3, wrg.getKtp().getKtp());
            stat.setString(4, wrg.getStatus());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public List<StatusIuran> loadWargaBelumBayar() throws SQLException {
        List<StatusIuran> statusIuranList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT\n"
                    + "statusiuran.`status`,\n"
                    + "statusiuran.`idStatusIuran`,\n"
                    + "statusiuran.tanggal,\n"
                    + "warga.nama,\n"
                    + "warga.noHp\n"
                    + "FROM\n"
                    + "statusiuran\n"
                    + "INNER JOIN warga ON statusiuran.ktp = warga.ktp\n"
                    + "WHERE statusiuran.`status` = \"Belum Bayar\";");
            statusIuranList = new ArrayList<>();
            while (rs.next()) {
                StatusIuran si = new StatusIuran();
                si.setStatus(rs.getString("status"));
                si.setIdStatusIuran(rs.getInt("idStatusIuran"));
                si.setTanggal(rs.getString("tanggal"));

                Warga wrg = new Warga();
                wrg.setNama(rs.getString("nama"));
                wrg.setNoHp(rs.getString("noHp"));

                si.setKtp(wrg);
                statusIuranList.add(si);

            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return statusIuranList;
    }

    public int ubahIuranWarga(StatusIuran siu) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {

            PreparedStatement stat = con.prepareStatement("UPDATE statusiuran SET tanggal = ? , status = ? WHERE idStatusIuran = ? ");
            stat.setString(1, siu.getTanggal());
            stat.setString(2, siu.getStatus());
            stat.setInt(3, siu.getIdStatusIuran());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public List<StatusIuran> loadWargaSudahBayar() throws SQLException {
        List<StatusIuran> statusIuranList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT\n"
                    + "jenisiuran.namaIuran,\n"
                    + "jenisiuran.biaya,\n"
                    + "warga.nama,\n"
                    + "statusiuran.tanggal,\n"
                    + "statusiuran.`status`\n"
                    + "FROM\n"
                    + "statusiuran\n"
                    + "INNER JOIN warga ON statusiuran.ktp = warga.ktp\n"
                    + "INNER JOIN jenisiuran ON statusiuran.idIuran = jenisiuran.idIuran\n"
                    + "WHERE statusiuran.status = \"Sudah Bayar\"");
            statusIuranList = new ArrayList<>();
            while (rs.next()) {
                StatusIuran si = new StatusIuran();
                si.setStatus(rs.getString("status"));
                si.setTanggal(rs.getString("tanggal"));

                Warga wrg = new Warga();
                wrg.setNama(rs.getString("nama"));
               
                si.setKtp(wrg);
                
                
                JenisIuran ji  = new JenisIuran();
                ji.setNamaIuran(rs.getString("namaIuran"));
                ji.setBiaya(rs.getInt("biaya"));
                si.setJenisIuran(ji);
                statusIuranList.add(si);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return statusIuranList;
    }
}
