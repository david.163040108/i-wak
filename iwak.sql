-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2018 at 03:20 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iwak`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenisiuran`
--

CREATE TABLE `jenisiuran` (
  `idIuran` int(10) NOT NULL,
  `namaIuran` varchar(30) NOT NULL,
  `biaya` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenisiuran`
--

INSERT INTO `jenisiuran` (`idIuran`, `namaIuran`, `biaya`) VALUES
(1, 'Iuran Bulanan', 10000),
(2, 'Iuran 17 Agustus', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `statusiuran`
--

CREATE TABLE `statusiuran` (
  `idStatusIuran` int(10) NOT NULL,
  `idIuran` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `ktp` varchar(12) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statusiuran`
--

INSERT INTO `statusiuran` (`idStatusIuran`, `idIuran`, `tanggal`, `ktp`, `status`) VALUES
(24, 1, '2018-12-01', '163040108', 'Sudah Bayar'),
(25, 1, '2019-01-01', '163040109', 'Belum Bayar'),
(26, 1, '2019-01-01', '163040110', 'Belum Bayar'),
(27, 1, '2019-02-01', '163040108', 'Belum Bayar'),
(28, 1, '2019-02-01', '163040109', 'Belum Bayar'),
(29, 1, '2019-02-01', '163040110', 'Belum Bayar');

-- --------------------------------------------------------

--
-- Table structure for table `warga`
--

CREATE TABLE `warga` (
  `ktp` varchar(13) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `noRumah` varchar(30) NOT NULL,
  `noHp` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warga`
--

INSERT INTO `warga` (`ktp`, `nama`, `noRumah`, `noHp`) VALUES
('163040108', 'David Priyadi', 'D12', '083875170292'),
('163040109', 'Daffa Mukti Z', 'D14', '082121216397'),
('163040110', 'Okta', 'D15', '083111111111');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jenisiuran`
--
ALTER TABLE `jenisiuran`
  ADD PRIMARY KEY (`idIuran`);

--
-- Indexes for table `statusiuran`
--
ALTER TABLE `statusiuran`
  ADD PRIMARY KEY (`idStatusIuran`),
  ADD KEY `ktp` (`ktp`),
  ADD KEY `idIuran` (`idIuran`);

--
-- Indexes for table `warga`
--
ALTER TABLE `warga`
  ADD PRIMARY KEY (`ktp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jenisiuran`
--
ALTER TABLE `jenisiuran`
  MODIFY `idIuran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statusiuran`
--
ALTER TABLE `statusiuran`
  MODIFY `idStatusIuran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `statusiuran`
--
ALTER TABLE `statusiuran`
  ADD CONSTRAINT `statusiuran_ibfk_1` FOREIGN KEY (`ktp`) REFERENCES `warga` (`ktp`),
  ADD CONSTRAINT `statusiuran_ibfk_2` FOREIGN KEY (`idIuran`) REFERENCES `jenisiuran` (`idIuran`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
